'use strict'

import Config from 'Config'

class LocationServiceProcessor {
  process(service, applicationId, instanceId) {
    return this.getLocation()
    .then(results => {
      return this.sendData(results, applicationId, instanceId)
    })
  }

  sendData (coords, applicationId, instanceId) {
    return fetch(Config.apiUrl + "/applications/" + applicationId + "/results/" + instanceId, {
      "method": "POST",
      "headers": {
        'content-type': 'application/json'
      },
      "body": JSON.stringify({
        "services": {
          "location": {
            "latitude": coords.latitude,
            "longitude": coords.longitude,
            "accuracy": coords.accuracy
          }
        }
      })
    })
    .then(response => {
    })
    .catch(err => {
      console.log(err)
    })
  }

  getLocation() {
    return new Promise((resolve, reject) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(result => {
          resolve(result.coords)
        })
      } else {
        reject("Geolocation not supported")
      }
    })
  }
}

export default LocationServiceProcessor
