'use strict'

import Config from 'Config'

class DeviceInfoServiceProcessor {
  process(service, applicationId, instanceId) {
    return this.sendData(applicationId, instanceId)
  }

  sendData (applicationId, instanceId) {
    return fetch(Config.apiUrl + "/applications/" + applicationId + "/results/" + instanceId, {
      "method": "POST",
      "headers": {
        'content-type': 'application/json'
      },
      "body": JSON.stringify({
        "services": {
          "device-information": {
            "model": this.deviceModel()
          }
        }
      })
    })
    .then(response => {
    })
    .catch(err => {
      console.log(err)
    })
  }

  deviceModel() {
      if (navigator.userAgent.match(/Android/i)) {
        return "Android"
      }
      if (navigator.userAgent.match(/BlackBerry/i)) {
        return "BlackBerry"
      }
      if (navigator.userAgent.match(/iPhone/i)) {
        return "iPhone"
      }
      if (navigator.userAgent.match(/iPad/i)) {
        return "iPad"
      }
      if (navigator.userAgent.match(/IEMobile/i)) {
        return "Windows Phone"
      }
  }
}

export default DeviceInfoServiceProcessor
