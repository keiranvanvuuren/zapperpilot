'use strict'

import Config from 'Config'

class CustomerBasicServiceProcessor {
  process(customerInfo, applicationId, instanceId) {
    return this.sendData(customerInfo, applicationId, instanceId)
  }

  sendData (customerInfo, applicationId, instanceId) {
    return fetch(Config.apiUrl + "/applications/" + applicationId + "/results/" + instanceId, {
      "method": "POST",
      "headers": {
        'content-type': 'application/json'
      },
      "body": JSON.stringify({
        "services": {
          "customer-basic": customerInfo
        }
      })
    })
    .then(response => {
    })
    .catch(err => {
      console.log(err)
    })
  }
}

export default CustomerBasicServiceProcessor
