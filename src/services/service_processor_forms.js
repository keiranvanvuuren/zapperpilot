'use strict'

import Config from 'Config'

class FormsServiceProcessor {
  process(data_fields, applicationId, instanceId) {
    return this.sendData(data_fields, applicationId, instanceId)
  }

  sendData (data_fields, applicationId, instanceId) {
    return fetch(Config.apiUrl + "/applications/" + applicationId + "/results/" + instanceId, {
      "method": "POST",
      "headers": {
        'content-type': 'application/json'
      },
      "body": JSON.stringify({
        "services": {
          "forms": {
            "form_response": data_fields.form_response
          }
        }
      })
    })
    .then(response => {
    })
    .catch(err => {
      console.log(err)
    })
  }
}

export default FormsServiceProcessor
