'use strict'

import Config from 'Config'

class WeatherServiceProcessor {
  process(service, applicationId, instanceId) {
    this.getCoords()
    .then(position => {
      this.getWeather(position, applicationId, instanceId)
    })
  }

  getWeather(position, applicationId, instanceId) {
    fetch("https://api.openweathermap.org/data/2.5/weather?appid=a82277044410f5b2e4c12153c6d1084a&lat=" + position.coords.latitude + "&lon=" + position.coords.longitude)
    .then(response => {
      return response.json()
    })
    .then(json => {
      this.sendData(json, applicationId, instanceId)
    })
  }

  getCoords() {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(position => {
        resolve(position)
      }, null, {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000})
    })
  }

  sendData (weather, applicationId, instanceId) {
    console.log(weather)
    fetch(Config.apiUrl + "/applications/" + applicationId + "/results/" + instanceId, {
      "method": "POST",
      "headers": {
        'content-type': 'application/json'
      },
      "body": JSON.stringify({
        "services": {
          "weather": {
            "temp": weather.main.temp,
            "humidity": weather.main.humidity,
            "pressure": weather.main.pressure,
            "temp_min": weather.main.temp_min,
            "temp_max": weather.main.temp_max
          }
        }
      })
    })
    .then(response => {
    })
    .catch(err => {
      console.log(err)
    })
  }
}

export default WeatherServiceProcessor
