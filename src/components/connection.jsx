'use strict'

import Config from 'Config'
import io from 'socket.io-client'
import React from 'react'

const ConnectionStates = {
  disconnected: "disconnected", connected: "connected", connecting: "connecting"
}

const Connection = React.createClass({
  getInitialState() {
    return {
      "connection_state": ConnectionStates.disconnected
    }
  },

  componentDidMount() {
    this.connect()
  },

  componentDidUpdate() {
    if (this.props.onConnectionStateChange) {
      this.props.onConnectionStateChange(this.state.connection_state)
    }
  },

  render() {
    return (
      <div>
      </div>
    )
  },

  connect() {
    let socket = io(Config.apiUrl)
    socket.on('connect', this.onConnected)
    socket.on('disconnect', this.onDisconnected)

    this.setState({
      socket: socket,
      connection_state: ConnectionStates.connecting
    })
  },

  onConnected() {
    this.setState({
      "connection_state": ConnectionStates.connected
    })

    if (this.props.onConnected) {
      this.props.onConnected(this.state.socket)
    }
  },

  onDisconnected() {
    this.setState({
      "connection_state": ConnectionStates.disconnected
    })
  }
})

export default Connection
