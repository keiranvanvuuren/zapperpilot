'use strict'

import CustomerBasicServiceProcessor from '../services/service_processor_customer_basic'
import React from 'react'
import { IonButton, IonFooter, IonItem, IonList } from 'reactionic'
import FormsServiceProcessor from '../services/service_processor_forms'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

const styles = {
  field: {  },
  input: { width:'100%', fontSize:16 },
  textArea: { width:'100%', fontSize:16 },
  sendButton: { borderRadius: '28px', width:'100%', height:48, color: '#005CB9', borderColor:'#005CB9', fontSize:'1.05em' },
  sendingButton: { borderRadius: '28px', width:'100%', height:48, color: '#E7E7E7', borderColor:'#E7E7E7', fontSize:'1.05em' }
}

const GetWellForm = React.createClass({
  getInitialState() {
    return {
      isSending: false,
      applicationId: "",
      scanInstanceId: "",
      form_fields: [],
      data_fields: {
        form_response: {}
      },
      success: false
    }
  },

  componentDidMount() {
    this.setState({
      form_fields: this.props.formFields
    })

    window.addEventListener('resize', () => {
      const focusedElement = document.querySelector('input:focus, textarea:focus')
      const content = document.querySelector('.content')

      if (focusedElement != null) {
        let elTop = focusedElement.getBoundingClientRect().top
        window.setTimeout(() => {
          console.log(content.scrollTop, elTop)
          if (content.scrollTop < elTop) {
            content.scrollTop += (elTop - 170)
          }
        }, 300)
      }
    })
  },

  renderField(field) {
    let input = this.createInputForField(field)

    return (
      <IonItem input key={field.id}>
        {input}
      </IonItem>
    )
  },

  renderFieldList() {
    return (
      <IonList key="field_list">
        {
          this.state.form_fields.map(ff => {
            return this.renderField(ff)
          })
        }
      </IonList>
    )
  },

  renderThankYou() {
    return (
      <div key="thankyou" className="thankyou-container">
        <div>
          <div>Thank You!</div>
          <p>Your message has been sent.</p>
        </div>
      </div>
    )
  },

  render() {
    return (
      <div className="content overflow-scroll" style={{padding:'0 40px', border:'none', top:100}}>
        {
          this.state.success ?
          this.renderThankYou() :
          this.renderFieldList()
        }
        <div style={{textAlign:'center', width:'100%', padding:'32px 0 0'}}>
          {this.sendButton()}
        </div>
      </div>
    )
  },

  sendButton() {
    if (this.state.success) return

    if (!this.state.isSending && this.formIsValid()) {
      return <button style={styles.sendButton} className="button button-outline" onClick={this.sendMessage}>Send</button>
    } else {
      return <button className="button button-outline" style={styles.sendingButton}>Send</button>
    }
  },

  createInputForField(field) {
    let input = ""

    if (field.type === "text") {
      input = <div style={{width:'100%'}}>
        <label>{field.label}</label>
        <input value={this.fieldValue(field.id)} style={styles.input} type="text" onChange={this.fieldChanged.bind(this, field.id, field.label)} />
      </div>
    } else if (field.type === "textarea") {
      input = <div style={{width:'100%'}}>
        <label>{field.label}</label>
        <textarea value={this.fieldValue(field.id)} rows={6} style={styles.input} onChange={this.fieldChanged.bind(this, field.id, field.label)}></textarea>
      </div>
    } else if (field.type === "checkbox") {
      input = <label>
        <input type="checkbox" onChange={this.fieldChanged.bind(this, field.id)} /> {field.label}
      </label>
    }

    return input
  },

  fieldValue(id) {
    let field = this.state.data_fields.form_response[id]

    if (field && field.value) {
      return field.value
    }

    return ""
  },

  formIsValid() {
    for (const f of this.state.form_fields) {
      if (!this.state.data_fields.form_response[f.id] || this.state.data_fields.form_response[f.id].value.trim() === "") {
        return false
      }
    }

    return true
  },

  fieldChanged(id, label, ev) {
    let fields = this.state.data_fields
    fields.form_response[id] = {
      label: label,
      value: ev.target.value
    }

    this.setState({
      data_fields: fields
    })
  },

  sendMessage() {
    if (!this.formIsValid()) {
      return
    }

    this.setState({
      isSending: true
    })

    new FormsServiceProcessor().process(this.state.data_fields, this.props.applicationId, this.props.scanInstanceId)
    .then(() => {
      let customerName = ""
      let customerNameField = null

      for (const field in this.state.data_fields.form_response) {
        if (this.state.data_fields.form_response[field].label.toLowerCase() === "from") {
          customerNameField = this.state.data_fields.form_response[field]
          break
        }
      }

      if (customerNameField && customerNameField.value) {
        customerName = customerNameField.value
        return new CustomerBasicServiceProcessor().process({ "name": customerName }, this.props.applicationId, this.props.scanInstanceId)
      }

      console.log(customerName)
    })
    .then(() => {
      this.setState({
        isSending: false,
        success: true
      })
    })
    .catch(err => {
      console.log(err)
    })
  }
})

export default GetWellForm
