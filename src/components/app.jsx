'use strict'

import Config from 'Config'
import Connection from './connection'
import DeviceInfoServiceProcessor from '../services/service_processor_device_information'
import GetWellForm from './getwell_form'
import { IonBody, IonContent } from 'reactionic'
import LocationServiceProcessor from '../services/service_processor_location'
import React from 'react'
import Spinner from './spinner'
import WeatherServiceProcessor from '../services/service_processor_weather'

require("../app.scss")

const styles = {
  header: { fontFamily:'Pacifico', textAlign:'center', fontSize:28, paddingTop:30, paddingBottom:50, color:'#005CB9', lineHeight:'36px' }
}

const App = React.createClass({
  getInitialState() {
    return {
      application: null,
      scanInstanceId: "",
      loading: true
    }
  },

  componentDidMount() {

  },

  renderForm() {
    if (this.state.loading) {
      return <div className="content overflow-scroll">
        <div className="fullscreen-loader">
          <Spinner />
        </div>
      </div>
    }

    if (this.state.application != null) {
      const formAppService = this.state.application.services.find(x => x.key === "forms")
      return <div>
          <div style={styles.header}>
            Get Well Soon<br />David
          </div>
          <GetWellForm applicationId={this.props.params.id} scanInstanceId={this.state.scanInstanceId} formFields={formAppService.application_fields.form_fields} />
        </div>
    }

    return
  },

  render() {
    return (
      <IonBody location={this.props.location} >
        <Connection onConnected={this.handleConnected} />
        {this.renderForm()}
      </IonBody>
    )
  },

  handleConnected(socket) {
    this.getApplication(socket)
    .then(app => {
      return this.sendScanResult(app)
    })
    .then(app => {
      this.processServices(app)
    })
    .catch(err => {
      alert(err)
      this.setState({
        "loading": false
      })
    })
  },

  getApplication(socket) {
    return new Promise((resolve, reject) => {
      socket.on('application_details', app => {
        this.setState({
          application: app,
          loading: this.state.scanInstanceId !== "" ? false : true
        })

        resolve(app)
      })
      socket.emit('application_details', this.props.params.id)
    })
  },

  sendScanResult(app) {
    return new Promise((resolve, reject) => {
      fetch(Config.apiUrl + "/applications/" + app._id + "/results", {
        "method": "POST",
        "headers": {
          'content-type': 'application/json'
        },
        "body": ""
      })
      .then(response => {
        return response.text()
      })
      .then(text => {
        this.setState({
          "loading": this.state.application !== null ? false : true,
          "scanInstanceId": text.split("\"").join("")
        })

        resolve(app)
      })
      .catch(err => {
        reject(err)
      })
    })
  },

  processServices(app) {
    app.services.forEach(service => {
      this.processService(service)
    })
  },

  processService(service) {
    switch (service.key) {
      case "location":
        new LocationServiceProcessor().process(service, this.state.application._id, this.state.scanInstanceId)
        break
      case "device-information":
        new DeviceInfoServiceProcessor().process(service, this.state.application._id, this.state.scanInstanceId)
        break
      case "weather":
        new WeatherServiceProcessor().process(service, this.state.application._id, this.state.scanInstanceId)
        break
    }
  }
})

export default App
