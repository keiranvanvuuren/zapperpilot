import React from 'react'

const styles = {
  spinner: { animation: 'spin 0.8s linear infinite', WebkitAnimation: 'spin 0.8s linear infinite' }
}

const Spinner = React.createClass({
  render() {
    return (
      <div className="spinner">
        <svg style={styles.spinner} viewBox="0 0 64 64"><g><circle strokeWidth="2" strokeDasharray="128" strokeDashoffset="82" r="26" cx="32" cy="32" stroke="#005CB9" fill="none" transform="rotate(158.11 32 32)"></circle></g></svg>
      </div>
    )
  }
})

export default Spinner
