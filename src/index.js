import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/app'
import 'babel-polyfill'

import { Router, Route, hashHistory } from 'react-router'

const Config = require('Config')

ReactDOM.render((
  <Router history={hashHistory}>
    <Route path="/:id" component={App} />
  </Router>
), document.getElementById('root'));
