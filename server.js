var express = require('express');
var app = express();

var isDevelopment = (process.env.NODE_ENV !== 'production');

app.use(express.static('.'))
  .get('/', function (req, res) {
    res.sendFile('./index.html');
  }).listen(process.env.PORT || 4040, function (err) {
    if (err) { console.log(err) };
    else {
      console.log("Server running on port 4040")
    }
  });
