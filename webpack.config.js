var path = require('path')
var webpack = require('webpack')

module.exports = {
    entry: [
      'webpack-dev-server/client?http://0.0.0.0:4040',
      'webpack/hot/only-dev-server',
      './src/index'
    ],
    output: {
        path: path.join(__dirname, 'build'),
        filename: "bundle.js",
        publicPath: '/build/'
    },
    resolve: {
      extensions: ['', '.jsx', '.scss', '.js', '.json']
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.ProvidePlugin({
        'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
      })
    ],
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" },
            { test: /\.scss$/, loaders: ["style", "css", "sass"] },
            { test: /\.jsx?$/, loaders: ['react-hot', 'babel'], include: path.join(__dirname, 'src') },
            { test: /\.js$/, loaders: ['react-hot', 'babel'], include: path.join(__dirname, 'src') }
        ]
    },
    externals: {
      "Config": JSON.stringify({
        "apiUrl": "https://zapper-platform.herokuapp.com"
      })
    }
};
