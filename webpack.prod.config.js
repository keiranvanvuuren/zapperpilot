var path = require('path')
var webpack = require('webpack')
var ClosureCompilerPlugin = require('webpack-closure-compiler')

module.exports = {
    entry: './src/index',
    output: {
        path: path.join(__dirname, 'build'),
        filename: "bundle.js",
        publicPath: '/build/'
    },
    resolve: {
      extensions: ['', '.jsx', '.scss', '.js', '.json']
    },
    plugins: [
      new webpack.optimize.DedupePlugin(),
      new webpack.ProvidePlugin({
        'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
      }),
      new ClosureCompilerPlugin({
          compiler: {
          language_in: 'ECMASCRIPT6',
          language_out: 'ECMASCRIPT5',
          compilation_level: 'SIMPLE'
        },
        concurrency: 3,
      })
    ],
    module: {
        loaders: [
          { test: /\.jsx?$/, exclude: /(node_modules|bower_components)/, loader: 'babel?presets[]=es2015' },
          { test: /\.css$/, loader: "style!css" },
          { test: /\.scss$/, loader: 'style!css!sass?sourceMap' },
          { test: /\.jsx?$/, loaders: ['react-hot', 'babel'], include: path.join(__dirname, 'src') },
          { test: /\.js$/, loaders: ['react-hot', 'babel'], exclude: /node_modules/, include: path.join(__dirname, 'src') }
        ]
    },
    externals: {
      "Config": JSON.stringify({
        "apiUrl": "https://zapper-platform.herokuapp.com"
      })
    }
};
